const { loggerConfig, getCorrelationId } = require("./lib/logger.middleware");
const { responsesMiddleware } = require("./lib/responses/index");
module.exports.loggerConfig = loggerConfig;
module.exports.getCorrelationId = getCorrelationId;
module.exports.responsesMiddleware = responsesMiddleware;
