let loggerServiceName = "NO NAME";

const setLoggerServiceName = (name) => {
  loggerServiceName = name;
};

const getLoggerServiceName = () => {
  return loggerServiceName;
};

const getCorrelationId = (req) => {
  let correlationId = "ID MISSING";
  if (req.header("X-C4P-CID")) {
    correlationId = req.header("X-C4P-CID");
  } else if (req.headers.correlationid) {
    correlationId = req.headers.correlationid;
  } else {
    console.warn("X-C4P-CID is missing in header");
  }
  return correlationId;
};

const loggerMiddleware = async (req, res, next) => {
  const time = new Date();
  const start = time.getTime();

  const origin_path = req.headers.origin_path;
  const user_id = req.headers.user_id;
  const tenant_id = req.headers.tenant_id;
  const url = req.url;
  const method = req.method;
  const body = req.body;
  const query = req.query;
  const correlationId = getCorrelationId(req);

  let errorMessage = null;

  req.on("error", (error) => {
    errorMessage = error;
  });

  res.on("runtime-error", (error) => {
    errorMessage = error;
  });

  res.on("finish", () => {
    const time = new Date();
    end = time.getTime();

    const log = {
      correlationId,
      origin_path,
      url,
      method,
      user_id,
      tenant_id,
      body,
      query,
      proccessedTime: end - start,
      status: res.statusCode,
      type: getLoggerServiceName(),
    };

    if (!errorMessage) {
      console.log(JSON.stringify(log));
    } else {
      log.error = errorMessage;
      console.error(JSON.stringify(log));
    }
  });

  next();
};

const loggerConfig = (name) => {
  setLoggerServiceName(name);
  return loggerMiddleware;
};

module.exports.loggerConfig = loggerConfig;
module.exports.getCorrelationId = getCorrelationId;
