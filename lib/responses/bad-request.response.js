module.exports.badRequest = (req, res) => (error) => {
  res.emit('runtime-error', error)
  res.status(400);
  res.json(error);
};
