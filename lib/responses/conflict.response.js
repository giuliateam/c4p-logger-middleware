module.exports.conflict = (req, res) => (error) => {
  res.emit('runtime-error', error)
  res.status(409);
  res.json(error);
};
