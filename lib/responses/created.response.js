module.exports.created = (res) => (data = {}) => {
  res.status(201);
  res.json(data);
};
