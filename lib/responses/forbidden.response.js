module.exports.forbidden = (req, res) => (error) => {
  res.emit('runtime-error', error)
  res.status(403);
  res.json(error);
};
