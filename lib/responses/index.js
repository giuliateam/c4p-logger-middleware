const { ok } = require("./ok.response");
const { created } = require("./created.response");
const { noContent } = require("./no-content.response");
const { badRequest } = require("./bad-request.response");
const { conflict } = require("./conflict.response");
const { notFound } = require("./not-found.response");
const { unauthorized } = require("./unauthorized.response");
const { forbidden } = require("./forbidden.response");
const { serverError } = require("./server-error.response");

const responses = (req, res, next) => {
  res.ok = ok(res);
  res.created = created(res);
  res.noContent = noContent(res);
  res.badRequest = badRequest(req, res);
  res.conflict = conflict(req, res);
  res.notFound = notFound(req, res);
  res.unauthorized = unauthorized(req, res);
  res.forbidden = forbidden(req, res);
  res.serverError = serverError(req, res);
  next();
};
module.exports.responsesMiddleware = responses
