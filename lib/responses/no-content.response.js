module.exports.noContent = (res) => (data = {}) => {
  res.status(204);
};
