module.exports.notFound = (req, res) => (error) => {
  res.emit('runtime-error', error)
  res.status(404);
  res.json(error);
};
