module.exports.ok = (res) => (data = {}) => {
  res.status(200);
  res.json(data);
};
