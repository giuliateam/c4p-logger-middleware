module.exports.serverError = (req, res) => (error) => {
  res.status(500);
  res.emit("runtime-error", error);
  res.json(error);
};
