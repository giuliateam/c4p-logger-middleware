module.exports.unauthorized = (req, res) => (error) => {
  res.emit('runtime-error', error)
  res.status(401);
  res.json(error);
};
